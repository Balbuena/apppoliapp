package com.example.policia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.affectiva.android.affdex.sdk.detector.Detector;
import com.example.policia.database.DBHelper;

public class MainActivity extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 123;

    private Button start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Rutina que llena la base de datos, no es necesario volver a ejecutar a menos que la bd se actualice
        try {
            DBHelper helper = new DBHelper(this, "police", null, 1);
            SQLiteDatabase database = helper.getWritableDatabase();

            if(database != null) {
                database.execSQL("INSERT INTO question VALUES(1 , '¿Sabe por que esta detenido?')");
                database.execSQL("INSERT INTO question VALUES(2 , '¿Que edad tiene y a que actividad se dedica?')");
                database.execSQL("INSERT INTO question VALUES(3 , '¿En donde estaba el dia de ayer?')");
                database.execSQL("INSERT INTO question VALUES(4 , '¿Que hacia ahi?')");
                database.execSQL("INSERT INTO question VALUES(5 , '¿Estaba usted en compañia?')");
                database.execSQL("INSERT INTO question VALUES(6 , '¿Es cierto que hubo una pelea?')");
                database.execSQL("INSERT INTO question VALUES(7 , '¿A que hora sucedio lo ocurrido?')");
                database.execSQL("INSERT INTO question VALUES(8 , '¿Es cierto que usted portaba un arma de fuego?')");
                database.execSQL("INSERT INTO question VALUES(9 , '¿Usted suele tomar alcohol?')");
                database.execSQL("INSERT INTO question VALUES(10 , '¿Como se describe usted como persona?')");
            }
        }
        catch (Exception e) {
            Log.e("err", e.toString());
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Explicamos porque necesitamos el permiso
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {

            }
            else {

                // El usuario no necesitas explicación, puedes solicitar el permiso:
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);

                //
            }
        }

        start = findViewById(R.id.start);

//        Metodo para lanzar activity de escaner
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), AffdexActivity.class);
                startActivity(intent);
            }
        });
    }
}
