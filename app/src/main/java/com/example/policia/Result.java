package com.example.policia;

public class Result {
    private String question;
    private double anger;
    private double disgust;
    private double fear;

    public Result(String question, double anger, double disgust, double fear) {
        this.question = question;
        this.anger = anger;
        this.disgust = disgust;
        this.fear = fear;
    }

    public String getQuestion() {
        return question;
    }

    public double getAnger() {
        return anger;
    }

    public double getDisgust() {
        return disgust;
    }

    public double getFear() {
        return fear;
    }
}
